# Waveguide Structure Generation

Utilities for generating the finite grid structures of index perturbed
dielectric waveguides (structures derived from a simple slab waveguide
by adding bands of index perturbation into the core region), and the
2D interpretation thereof.

```@autodocs
Modules = [FDMGridWaveguides]
Pages   = ["waveguide_gen.jl"]
```
