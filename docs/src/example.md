# Example Usage

The basic application of this package is the creation of index structured
waveguides. As a really basic example,
let's try to create the structure shown in Figure 2.3 of
["Semiconductor Laser Mode Engineering via Waveguide Index Structuring"](http://hdl.handle.net/2142/102511)
and plot it using the `Plots` package:

```@example 1d
import FDMGridWaveguides
import Plots: plot, heatmap
import Plots: savefig # hide

# Define the structure parameters (index values and gain region mask)
nclad, ncore, npert=3, 3.01, 3.009
aclad, acore, apert=0, 1, 1

# Cladding width
wclad=10

# Width of core and perturbation regions
wbands=[14, 2, 14]

# We generate the overall structure "band description"
(w, n, a) =
    FDMGridWaveguides.mkbandedwvg(nclad, ncore, npert, aclad, acore, apert, wclad, wbands)

# And now we generate the finitely sampled structure
lambda0 = 1
(index, dx, active) = FDMGridWaveguides.widths2index(w, n, a, lambda0)

# And we plot
x=cumsum(dx); x.-=x[end]/2
plot(x, index,
	xlabel="x [Wavelengths]",
	ylabel="Real Refractive Index",
	legend=false,
)
savefig("index_1d.png"); nothing # hide
```

![Resulting Refractive Index Structure](index_1d.png)

We can easily make more complicated structures by modifying the `wbands`
array:

```@example 1d
# Cladding width
wclad=5

# Width of core and perturbation regions
wbands=[1:3; 1; 3:-1:1]

# We generate the overall structure "band description"
(w, n, a) =
    FDMGridWaveguides.mkbandedwvg(nclad, ncore, npert, aclad, acore, apert, wclad, wbands)

# And now we generate the finitely sampled structure
(index, dx, active) = FDMGridWaveguides.widths2index(w, n, a, lambda0)

# And we plot
x=cumsum(dx); x.-=x[end]/2
plot(x, index,
	xlabel="x [Wavelengths]",
	ylabel="Real Refractive Index",
	legend=false,
)
savefig("index_1d_complicated.png"); nothing # hide
```

![Resulting Refractive Index Structure](index_1d_complicated.png)

Now, we can try to implement these structures as etched perturbations to
a rib waveguide:

```@example 1d
# Define the structure parameters (index values and gain region mask)
nclad, ncore, npert=[1.5, 3.01, 1, 1, 1], [1.5, 3.01, 3.01, 3.01, 1], [1.5, 3.01, 3.01, 1, 1]
aclad, acore, apert=[0, 1, 0, 0, 0], [0, 1, 1, 1, 0], [0, 1, 1, 0, 0]

# Width of core and perturbation regions
wbands=[1:3; 1; 3:-1:1]

# Width of epitaxial layers
w_epi=[1, 0.1, 0.3, 0.1, 1]

# We generate the overall structure "band description"
(w, n, a) =
    FDMGridWaveguides.mkbandedwvg(nclad, ncore, npert, aclad, acore, apert, wclad, wbands)

# And now we generate the finitely sampled structure
(index, dx_epi, dx_lat, active) = FDMGridWaveguides.widths2index(w, w_epi, n, a, lambda0)

# And we plot
x=cumsum(dx_lat); x.-=x[end]/2
y=cumsum(dx_epi)
heatmap(x, y, index,
	xlabel="x [Wavelengths]",
	ylabel="y [Wavelengths]",
	title="Real Refractive Index",
)
savefig("index_2d_complicated.png"); nothing # hide
```

![Resulting Refractive Index Structure](index_2d_complicated.png)
