# References

The code for package was originally made to parametrically generate
dielectric waveguides with index perturbations in the core, including
those in the conference paper
["Mode Engineering via Waveguide Structuring"](https://doi.org/10.1109/islc.2018.8516196)
and my MS thesis,
["Semiconductor Laser Mode Engineering via Waveguide Index Structuring"](http://hdl.handle.net/2142/102511),
in order to simulate them using finite-difference mode-solving
(the code which has evolved into the
[`FDMModes`](https://gitlab.com/pawelstrzebonski/FDMModes.jl)
package).
While those publications use 1D waveguide modeling, this code has been
expanded to generate 2D waveguide structures based on the same concept
of index structuring. This enables modeling the transverse structure
of edge-emitting lasers implementing index structuring for mode control
(not published at time of writing).

That being said, as (if) other structures become of interest and utilities
for them are implemented in this package, other references should be provided.
