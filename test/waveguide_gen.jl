@test FDMGridWaveguides.trunc_arr(ones(10), 5) == ones(5)
@test FDMGridWaveguides.trunc_arr(1:5, 5) == [1, 2, 2]


# Verify selector matrix implements the desired repetition when multiplying
nreps = [1, 2, 3]
sm = FDMGridWaveguides.selectormatrix(nreps)
@test sm * nreps == [1, 2, 2, 3, 3, 3]


# Generate 1D waveguide description
nclad, ncore, npert = 1, 3, 2
aclad, acore, apert = 0, 1, 0.5
wclad = 10
wbands = [1, 2, 3, 4]
(w, n, a) =
    FDMGridWaveguides.mkbandedwvg(nclad, ncore, npert, aclad, acore, apert, wclad, wbands)

# Test that cladding is added
@test w[1] == w[end] == wclad
@test n[1] == n[end] == nclad
@test a[1] == a[end] == aclad

# Sanity test for output size
@test length(w) == length(wbands) + 2
@test length(w) == length(n) == length(a)

# Check ordering/value of inner values
@test w[2:end-1] == wbands
@test all(n[2:2:end-1] .== ncore)
@test all(n[3:2:end-1] .== npert)
@test all(a[2:2:end-1] .== acore)
@test all(a[3:2:end-1] .== apert)


# Generate 1D waveguide on a FDM grid
lambda0 = 1
(index, dx, active) = FDMGridWaveguides.widths2index(w, n, a, lambda0)

# Sanity check on output size
@test length(index) == length(dx) == length(active)

# Sampling should be below a certain size
@test all(dx .<= lambda0 / (10 * maximum(n)))

# Output values should be from input values
@test unique(n) == unique(index)
@test unique(a) == unique(active)


# Generate 2D waveguide description
nclad, ncore, npert = [1, 1, 1], [1, 3, 1], [1, 2, 1]
aclad, acore, apert = [0, 0, 0], [0, 1, 0], [0, 0.5, 0]
wclad = 10
wbands = [1, 2, 3, 4]
(w, n, a) =
    FDMGridWaveguides.mkbandedwvg(nclad, ncore, npert, aclad, acore, apert, wclad, wbands)

# Test that cladding is added
@test w[1] == w[end] == wclad
@test n[:, 1] == n[:, end] == nclad
@test a[:, 1] == a[:, end] == aclad

# Sanity test for output size
@test length(w) == length(wbands) + 2
@test length(w) == size(n, 2) == size(a, 2)
@test size(n) == size(a)

# Check ordering/value of inner values
@test w[2:end-1] == wbands
@test all([x == ncore for x in eachcol(n[:, 2:2:end-1])])
@test all([x == npert for x in eachcol(n[:, 3:2:end-1])])
@test all([x == acore for x in eachcol(a[:, 2:2:end-1])])
@test all([x == apert for x in eachcol(a[:, 3:2:end-1])])


# Generate 2D waveguide on a FDM grid
lambda0 = 1
w_epi = [3, 1, 3]
(index, dx_epi, dx_lat, active) = FDMGridWaveguides.widths2index(w, w_epi, n, a, lambda0)

# Sanity check on output size
@test size(index) == size(active)
@test size(index, 2) == length(dx_lat)
@test size(index, 1) == length(dx_epi)

# Sampling should be below a certain size
@test all(dx_lat .<= lambda0 / (10 * maximum(n)))
@test all(dx_epi .<= lambda0 / (10 * maximum(n)))

# Output values should be from input values
@test unique(n) == unique(index)
@test unique(a) == unique(active)

#TODO: Test effects of dxmax, more thorough test of outputs
